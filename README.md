# Experimenting with Perlin Noise

This repo includes an implementation of the perlin noise algorithm in rust and some particle simulation on the induced force field.

Additionaly primitive drawing methods have been implemented (lines & circles) as helper functions. 

Heavilly inspired by following article:

https://sighack.com/post/getting-creative-with-perlin-noise-fields

Result:

![result](outputs/animated.gif)
 

## How to run

`cargo run` in the root folder.