
use image::{ImageBuffer};

use crate::{perlin::{Vector2, perlin_noise}, draw};

const NOISE_SCALE: f64 = 0.01;
const FORCE_SCALE: f64 = 1.0;
const PI: f64 = 3.14;

pub fn _draw_force_field(buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>>){
    for x in (10..=buffer.width()-20).rev().step_by(20){
        for y in (10..=buffer.height()-20).rev().step_by(20){
            let force_vector = force_at(x as f64, y as  f64);
            _draw_force_vector(force_vector, x as f64, y as f64, 15.0, buffer);
        }
    }
}
    

fn _draw_force_vector(force: Vector2, x: f64, y: f64, len: f64, buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>>){
    draw::_circle(&Vector2 { x: x, y: y }, 3.0, buffer);
    draw::line(Vector2 { x: x, y }, Vector2 { x: x + force.x * len, y: y + force.y* len }, buffer);
}


fn force_from_angle(theta: f64) -> Vector2{
    Vector2{
        x: theta.cos() * FORCE_SCALE,
        y: theta.sin() * FORCE_SCALE
    }
}

pub fn force_at(x: f64, y: f64) -> Vector2{
    let theta = perlin_noise(x * NOISE_SCALE, y* NOISE_SCALE) * 2.0 * PI;
    force_from_angle(theta)
}