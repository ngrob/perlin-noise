use image::ImageBuffer;

use crate::perlin::Vector2;

fn lerp(start: f64, end: f64, t: f64) -> f64 {
    start * (1.0 - t) + end * t
}

fn round_point(p: Vector2) -> Vector2{
    Vector2 { x: p.x.round(), y: p.y.round() }
}

fn diagonal_distance(p0: Vector2, p1: Vector2) -> f64{
    let dx = p1.x -p0.x;
    let dy = p1.y - p0.y;
    dx.abs().max(dy.abs())

}
fn lerp_point(p0: Vector2, p1: Vector2, t:f64) -> Vector2{
    let lerp_x = lerp(p0.x, p1.x, t);
    let lerp_y = lerp(p0.y, p1.y , t);
    Vector2 { x: lerp_x, y: lerp_y }
}

// https://www.redblobgames.com/grids/line-drawing.html
pub fn line(start: Vector2, end:Vector2, buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>>){
    let mut points: Vec<Vector2> = Vec::new();
    let n = diagonal_distance(start, end).round() as i32;
    for step in 0..n {
        let t: f64;
        if n == 0 {
            t = 0.0;
        }else{
            t = step as f64 / n as f64;
        }
        points.push(round_point(lerp_point(start, end, t)));
    }

    for point in points{
        let pixel = buffer.get_pixel_mut(point.x as u32, point.y as u32);
        *pixel = image::Rgba([0, 0, 0, 255]);
    }
}


fn _inside_circle(center: &Vector2, p0: Vector2, radius: f64) -> bool {
    let dx = center.x - p0.x;
    let dy = center.y - p0.y;
    let distance = (dx * dx + dy * dy).sqrt();
    distance <= radius 
}

pub fn _circle(p0: &Vector2, radius: f64, buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>> ){
   let top = (p0.y - radius).ceil() as  u32; 
   let bottom = (p0.y + radius).floor()  as  u32; 
   let left = (p0.x - radius).ceil()  as  u32;
   let right = (p0.x + radius).floor()  as  u32;

   for y in top..bottom{
    for x in left..right{
        if _inside_circle(p0, Vector2{x: x as f64, y: y as f64}, radius) {
            let pixel = buffer.get_pixel_mut(x, y);
            *pixel = image::Rgba([255, 255, 255, 255]);
        }
    }
   }
}

pub fn background(buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>>, color: image::Rgba<u8>){
    for (_ ,_, pixel) in buffer.enumerate_pixels_mut(){
        *pixel = color;
    }
}