use image::ImageBuffer;
use rand::Rng;

use crate::{perlin::Vector2, field::force_at, WIDTH, HEIGHT, draw};
#[derive(Copy, Clone)]
struct Particle{
    prev_position: Vector2,
    position: Vector2,
    dead: bool,
    lifetime: u32,
}


impl Particle{
    pub fn update(mut self) ->Self{
        let force: Vector2 = force_at(self.position.x, self.position.y);
        self.lifetime -= 1;

        if self.lifetime <= 0{
            self.dead = true;
        }


        if self.position.x >= (WIDTH as f64 -1.0){
            self.position.x -= WIDTH as f64 -1.0;
            self.prev_position.x -= WIDTH as f64 -1.0 ;
        }else if self.position.x <= 0.0{
            self.position.x += WIDTH as f64 -1.0;
            self.prev_position.x += WIDTH as f64 -1.0;
        }

        if self.position.y >= (HEIGHT as f64 -1.0){
            self.position.y -= HEIGHT as f64 -1.0;
            self.prev_position.y -= HEIGHT as f64 - 1.0;
        }else if self.position.y <= 0.0{
            self.position.y += HEIGHT as f64 -1.0;
            self.prev_position.y += HEIGHT as f64 -1.0;
        }

        self.prev_position = self.position;
        self.position = Vector2{
            x: self.position.x + force.x,
            y: self.position.y + force.y
        };
        self
    }


}
#[derive(Clone)]
pub struct ParticleSet{
    particles: Vec<Particle>,
    pub dead: bool,

}

impl ParticleSet {
    pub fn new(n: u32, lifetime: u32) -> ParticleSet{
        let mut particles: Vec<Particle> = Vec::new();
        for _ in 0..n{
            let x : u32 = rand::thread_rng().gen_range(0..=WIDTH-1);
            let y : u32 = rand::thread_rng().gen_range(0..=HEIGHT-1);
            let pos = Vector2{x: x as f64, y: y as  f64};
            particles.push(Particle{position: pos, prev_position: pos, dead: false, lifetime: lifetime});
        }
        let dead = false;
        ParticleSet { particles, dead }
    }

    pub fn update(mut self) -> Self{
        self.dead = true;
        for i in 0..self.particles.len() - 1{
            let p: Particle = self.particles[i];
            if p.dead {
                continue;
            }
            self.dead = false;
            self.particles[i] = p.update();
        }
        self
    }

    pub fn draw(mut self, buffer: &mut ImageBuffer<image::Rgba<u8>, Vec<u8>>) -> Self{
        self.dead = true;
        for i in 0..self.particles.len() - 1{
            let  p: Particle = self.particles[i];
            if p.dead {
                continue;
            }
            self.dead = false;
            draw::line(p.prev_position, p.position, buffer)
        }
        self
    }
}