mod draw;
mod field;
mod perlin;
mod particle;

// use std::fs::File;

pub const WIDTH: u32 = 1024;
pub const HEIGHT: u32= 1024;

fn main() {
    
    let img = &mut image::RgbaImage::new(WIDTH, HEIGHT);

    // Gif stuff
    // let mut image = File::create("animated.gif").unwrap();
    // let mut encoder = gif::Encoder::new(&mut image, WIDTH, HEIGHT, &[]).unwrap();
   
    draw::background(img, image::Rgba([255, 255, 255, 255]));
    let mut set = particle::ParticleSet::new(10000, 100);
    let mut is_dead = false;

    while !is_dead {
        set = set.draw(img);
        set = set.update();
        is_dead = set.dead;
        
        // Write iteration to gif frame
        // let frame = gif::Frame::from_rgba_speed(WIDTH, HEIGHT,  &mut *img.clone().into_raw(), 10);
        // encoder.write_frame(&frame).unwrap();
    }


    
    img.save("out.png").unwrap();
    
    println!("Finished!");


}
